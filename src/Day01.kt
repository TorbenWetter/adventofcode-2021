fun main() {
    fun part1(input: List<String>): Int {
        val depthMeasurements = input.map { it.toInt() }
        return depthMeasurements.mapIndexed { idx, value ->
            idx > 0 && value > depthMeasurements[idx - 1]
        }.count { b -> b }
    }

    fun part2(input: List<String>): Int {
        val depthMeasurements = input.map { it.toInt() }
        return depthMeasurements.mapIndexed { idx, value ->
            idx > 2 && value > depthMeasurements[idx - 3]
        }.count { b -> b }
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("Day01_test")
    check(part1(testInput) == 7)

    val input = readInput("Day01")
    println(part1(input))
    println(part2(input))
}
