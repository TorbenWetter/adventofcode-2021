fun main() {
    fun part1(input: List<String>): Int {
        return input.groupingBy { instruction -> instruction.substringBefore(" ") }
            .fold(0) { coordinate, instruction -> coordinate + instruction.substringAfter(" ").toInt() }
            .run { getValue("forward") * (getValue("down") - getValue("up")) }
    }

    fun part2(input: List<String>): Int {
        var aim = 0
        return input.fold(mutableMapOf("position" to 0, "depth" to 0)) { coordinates, instruction ->
            val units = instruction.substringAfter(" ").toInt()
            when (instruction.substringBefore(" ")) {
                "forward" -> {
                    coordinates["position"] = coordinates["position"]!! + units
                    coordinates["depth"] = coordinates["depth"]!! + units * aim
                }
                "down" -> aim += units
                "up" -> aim -= units
            }
            coordinates
        }.run { getValue("position") * getValue("depth") }
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("Day02_test")
    check(part1(testInput) == 150)

    val input = readInput("Day02")
    println(part1(input))
    println(part2(input))
}
