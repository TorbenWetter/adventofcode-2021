import kotlin.math.pow

fun main() {
    fun part1(input: List<String>): Int {
        return input.map { it.chunked(1).map { bit -> bit.toInt() } } // Convert 12-bit Strings to IntArrays
            .reduce { bitSums, bits -> bitSums.zip(bits, Int::plus) }.let { bitSums -> // Calculate vertical bit sums
                var gammaRating = 0
                var epsilonRating = 0
                bitSums.reversed().forEachIndexed { i, bitSum ->
                    when (bitSum.toFloat() / input.size > 0.5f) { // If probability > 0.5, it's the most common bit
                        true -> gammaRating += 2.0.pow(i).toInt()
                        false -> epsilonRating += 2.0.pow(i).toInt()
                    }
                }
                gammaRating * epsilonRating
            }
    }

    fun part2(input: List<String>): Int {
        return mapOf("oxygen" to true, "co2" to false).values.map { keepMostCommonBit ->
            val binaryNumbers = input.map { it.chunked(1).map { bit -> bit.toInt() } }.toMutableList()
            var idx = 0
            while (binaryNumbers.size > 1) {
                val mostCommonBit = if (binaryNumbers.sumOf { it[idx] }.toFloat() / binaryNumbers.size >= 0.5f) 1 else 0
                binaryNumbers.removeAll { it[idx] != if (keepMostCommonBit) mostCommonBit else 1 - mostCommonBit }
                idx++
            }
            binaryNumbers[0].reversed().foldIndexed(0) { i, rating, bit -> rating + bit * 2.0.pow(i).toInt() }
        }.reduce { totalRating, rating -> totalRating * rating }
    }

    // test if implementation meets criteria from the description, like:
    val testInput = readInput("Day03_test")
    check(part1(testInput) == 198)

    val input = readInput("Day03")
    println(part1(input))
    println(part2(input))
}
